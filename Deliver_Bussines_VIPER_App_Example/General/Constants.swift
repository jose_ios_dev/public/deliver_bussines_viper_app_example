//
//  Constants.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 17/03/24.
//

import Foundation
import UIKit

class Constants {
    static let APP_TITLE = "Deliver_Bussines_VIPER_App_Example"
    static let DATA_BASE = "Deliver_Bussines_VIPER_App_Example"
    static let SERVER_PRODUCTION_URL_PORT = "https://apiv4.ordering.co"
    static let SERVER_TEST_URL_PORT = "https://apiv4.ordering.co"
    static let SERVER_PLATFORM_HEADER = "iOS"
    static let APP_VERSION:String = {
        let path = Bundle.main.url(forResource: "Info", withExtension: "plist")
        let dic = NSDictionary(contentsOf: path!) as? [String: Any]
        let shortVersion:String=dic?["CFBundleShortVersionString"] as? String ?? ""
        let buildVersion:String=dic?["CFBundleVersion"] as? String ?? ""
        return (shortVersion)//+"."+buildVersion)
    }()
    static var UUID_DEVICE:String {
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            return uuid
        } else {
            return ""
        }
    }
    static let IS_IPAD_DEVICE:Bool = UIDevice.current.userInterfaceIdiom == .pad
    static let DEVICE_TYPE:UIUserInterfaceIdiom = UIDevice.current.userInterfaceIdiom
}
