//
//  Extensions.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

extension String {
    func localizable() -> String {
        return NSLocalizedString(self, comment: self)
    }

}
