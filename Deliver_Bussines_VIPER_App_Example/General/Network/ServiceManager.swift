//
//  ServiceManager.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 17/03/24.
//

import Foundation
import Alamofire
import Reachability

class ServiceManager {
    
    typealias MethodHandler0 = () -> Void
    typealias MethodHandler1 = (_ sampleParameter : Dictionary<String,Any>)  -> Void
    typealias MethodHandler2 = (_ sampleParameter : String)  -> Void
    
    let timeInterval = 30.0
    
    fileprivate func handle(response: AFDataResponse<Any>, _ correctAnswerHandler: @escaping MethodHandler1, _ incorrectAnswerHandler: @escaping MethodHandler2) {
        var json:[String:Any] = [:]
        
        switch response.result {
        case .success(let value):
            guard let responseValue = value as? [String: Any] else {
                
                if (response.response?.statusCode ?? 500) == 204 {
                    correctAnswerHandler([:])
                }else{
                    incorrectAnswerHandler(NSLocalizedString("ErrorData", comment: "Error data"))
                }
                return
            }
            json = responseValue
            break
        case .failure(let error):
            // error handling
            incorrectAnswerHandler(NSLocalizedString("UnreachableServer", comment: "Unreachable Server"))
            debugPrint(error)
            return
        }
        
        let statusResponse:Int = (response.response?.statusCode ?? 500)
        
        if json["error"] as? Bool ?? false {//ordering.co error validation
            let errors = json["result"] as? [String] ?? ["DefaultUknowError"]
            let errorMsg:String = errors.joined(separator: " \n ")
            incorrectAnswerHandler(errorMsg)
            return
        }
        
        switch statusResponse {
        case 500:
            incorrectAnswerHandler("Status Response \(statusResponse)")
        case 401:
            self.unauthorized(completion: {
                incorrectAnswerHandler("Status Response \(statusResponse)")
            })
        case 400,404:
            if let errorCode:Int=json["code"] as? Int {
                incorrectAnswerHandler(String(errorCode))
            }else{
                incorrectAnswerHandler("DefaultUknowError")
            }
        //                print("bad request")
        case 412:
            if let errorCode:Int=json["code"] as? Int {
                if errorCode == 4113 {//la app requiere actualizacion
                    self.updateAppRequired(errorCode: errorCode)
                }else{
                    incorrectAnswerHandler(String(errorCode))
                }
            }else{
                incorrectAnswerHandler("DefaultUknowError")
            }
        case 200,201,204:
            if let data = json["data"] as? Dictionary<String, Any> {
                correctAnswerHandler(data)
            } else {
                correctAnswerHandler(json)
            }
        default:
            if let serverErrorCode:Int = json["code"] as? Int {
                incorrectAnswerHandler("\(serverErrorCode)")//toma los codigos del localizable network errors
                return
            }
            incorrectAnswerHandler("Status Response \(statusResponse)")
        }
    }
    
    fileprivate func updateAppRequired(errorCode:Int) {
        
    }
    
    fileprivate func reachability() {
        
        //snackBar.build(text: NSLocalizedString("reachability", comment: "No internet")).show()
    }
    
    fileprivate func unauthorized(completion:MethodHandler0?) {
        
        
        guard let completion = completion else {
            return
        }
        completion()
    }
    
    func isConnectedViaWIFI(correctAnswerHandler: @escaping MethodHandler0, incorrectAnswerHandler: @escaping MethodHandler2) -> Bool {
        guard let reachability = try? Reachability() else{
            incorrectAnswerHandler("reachability")
            return false
        }
        
        if reachability.connection == .unavailable {
            incorrectAnswerHandler("reachability")
            self.reachability()
            return false
        }
        
        let isWifiConnected:Bool = reachability.connection == .wifi
        
        if !isWifiConnected {
            incorrectAnswerHandler("NotWifiConnection")
            return false
        }
        
        correctAnswerHandler()
        return isWifiConnected
    }
   
    
    func requestGet(url:String, correctAnswerHandler: @escaping MethodHandler1, incorrectAnswerHandler: @escaping MethodHandler2) -> DataRequest? {
        
        guard let reachability = try? Reachability() else{
            incorrectAnswerHandler("reachability")
            return nil
        }
        
        if reachability.connection == .unavailable {
            incorrectAnswerHandler("reachability")
            self.reachability()
            return nil
        }
        
        let urlRequest:URL = URL(string:url)!
        
        var request:URLRequest = URLRequest(url: urlRequest)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = self.timeInterval

        
        let  pendingRequest=AF.request(request).responseJSON {response in
            self.handle(response: response, correctAnswerHandler, incorrectAnswerHandler)
        }
        return pendingRequest
    }
    
    
    func reachabilitys(answerHandler:String){
        
        guard let reachability = try? Reachability() else{
            debugPrint("reachability")
            return
        }
        
        if reachability.connection == .unavailable {
            let json = Dictionary(dictionaryLiteral: ("detail","reachability"))
            NotificationCenter.default.post(name: NSNotification.Name(answerHandler), object:json)
        }
    }
    
    func isConnected() -> Bool{
        return NetworkReachabilityManager()!.isReachable
    }
    
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}


