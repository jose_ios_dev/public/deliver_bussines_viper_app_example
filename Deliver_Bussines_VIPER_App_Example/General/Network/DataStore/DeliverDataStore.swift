//
//  DeliverDataStore.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 17/03/24.
//

import UIKit

class DeliverDataStore: DataStore {
    var controller:DeliveryViewController?
    
    override init() {}
    
    init(_ controller:DeliveryViewController?) {
        if controller != nil{
            self.controller=controller
            controller!.showLoader()
        }
    }
    
    
    func getBussines(correctAnswer: @escaping CorrectHandler, errorAnswer: @escaping ErrorHandler) {
        let servicePrefix = dic["getBussiness"] as! String
        let webService = server + version + servicePrefix
        
        if controller != nil {
            controller!.pendingTasks.append(self.serviceManager.requestGet(url: webService, correctAnswerHandler: correctAnswer, incorrectAnswerHandler: errorAnswer))
        }else{
            _ = self.serviceManager.requestGet(url: webService, correctAnswerHandler: correctAnswer, incorrectAnswerHandler: errorAnswer)
        }
    }
    
}
