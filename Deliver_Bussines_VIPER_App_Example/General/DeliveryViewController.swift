//
//  DeliveryViewController.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 17/03/24.
//

import UIKit
import Alamofire

class DeliveryViewController: UIViewController, ViewProtocol {
    var presenter: PresenterProtocol?
    var loaderActicity: UIActivityIndicatorView!
    var pendingTasks:[DataRequest?]=[]
    let appDelegate=UIApplication.shared.delegate as! AppDelegate
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        debugPrint("<<<<<< memory warning")
    }
    
    func showLoader() {
        let sc=UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        if self.loaderActicity != nil {
            self.removeLoader()
        }
        self.loaderActicity = UIActivityIndicatorView()
        self.loaderActicity?.frame = sc!.frame//self.view.frame
        self.loaderActicity?.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
        self.loaderActicity.color = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        
        self.loaderActicity?.startAnimating()
        
        if self.navigationController?.isNavigationBarHidden ?? true == false {
            self.navigationController?.navigationBar.addSubview(self.loaderActicity)
        }else{
            self.view.addSubview(self.loaderActicity!)
        }
    }
    
   func removeLoader() {
        if(self.loaderActicity != nil){
                self.loaderActicity?.removeFromSuperview()
                self.loaderActicity = nil
        }
    }
    
    func observerError(_ error:String) {
        self.removeLoader()
        let okAction = UIAlertAction(title: "ok", style: .default)
        Utils.showSimpleAlert(title: Constants.APP_TITLE, message: error, controller: self, actions: [okAction], completion: nil)
    }
    
    func showMessage(_ message:String, actions:[UIAlertAction], completion:(() -> Void)?) {
        Utils.showSimpleAlert(title: Constants.APP_TITLE, message: message, controller: self, actions: actions, completion: completion)
    }

    deinit {
        debugPrint("<<<\(self)>>>")
    }

}
