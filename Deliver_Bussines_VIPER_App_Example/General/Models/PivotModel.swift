//
//  PivotModel.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class PivotModel {
    var businessId: Int
    var deliveryZoneId: Int
    
    init(_ dic: [String: Any]) {
        businessId = dic["business_id"] as? Int ?? 0
        deliveryZoneId = dic["delivery_zone_id"] as? Int ?? 0
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
