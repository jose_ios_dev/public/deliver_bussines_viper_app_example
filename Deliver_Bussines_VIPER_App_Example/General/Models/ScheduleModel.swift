//
//  ScheduleModel.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class ScheduleModel {
    var enabled: Bool
    var lapses: [LapseModel]
    
    init(_ dic: [String: Any]) {
        enabled = dic["enabled"] as? Bool ?? false
        lapses = (dic["lapses"] as? [[String: Any]] ?? []).compactMap { LapseModel($0) }
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
