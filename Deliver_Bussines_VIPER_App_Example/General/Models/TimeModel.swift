//
//  TimeModel.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class TimeModel {
    var hour: Int
    var minute: Int
    
    init(_ dic: [String: Any]) {
        hour = dic["hour"] as? Int ?? 0
        minute = dic["minute"] as? Int ?? 0
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
