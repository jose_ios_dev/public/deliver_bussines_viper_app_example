//
//  CoordinatesModel.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class CoordinatesModel {
    var lat: Double
    var lng: Double
    
    init(_ dic: [String: Any]) {
        lat = dic["lat"] as? Double ?? 0.0
        lng = dic["lng"] as? Double ?? 0.0
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
