//
//  ZoneModel.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class ZoneModel {
    var id: Int
    var businessId: Int
    var name: String
    var type: Int
    var address: String
    var data: ZoneDataModel
    var dropdownOptionId: Int?
    var price: Int
    var minimum: Int
    var schedule: [ScheduleModel]
    var enabled: Bool
    var hourlyDeliveryTimes: Any?
    var ownerType: String
    var pivot: PivotModel
    
    init(_ dic: [String: Any]) {
        id = dic["id"] as? Int ?? 0
        businessId = dic["business_id"] as? Int ?? 0
        name = dic["name"] as? String ?? ""
        type = dic["type"] as? Int ?? 0
        address = dic["address"] as? String ?? ""
        data = ZoneDataModel(dic["data"] as? [String: Any] ?? [:])
        dropdownOptionId = dic["dropdown_option_id"] as? Int
        price = dic["price"] as? Int ?? 0
        minimum = dic["minimum"] as? Int ?? 0
        schedule = (dic["schedule"] as? [[String: Any]] ?? []).compactMap { ScheduleModel($0) }
        enabled = dic["enabled"] as? Bool ?? false
        hourlyDeliveryTimes = dic["hourly_delivery_times"]
        ownerType = dic["owner_type"] as? String ?? ""
        pivot = PivotModel(dic["pivot"] as? [String: Any] ?? [:])
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
