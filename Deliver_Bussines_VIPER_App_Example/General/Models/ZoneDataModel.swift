//
//  ZoneDataModel.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class ZoneDataModel {
    var center: CoordinatesModel
    var radio: Double
    
    init(_ dic: [String: Any]) {
        center = CoordinatesModel(dic["center"] as? [String: Any] ?? [:])
        radio = dic["radio"] as? Double ?? 0.0
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
