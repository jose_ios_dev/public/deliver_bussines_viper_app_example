//
//  BussinesModel.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 17/03/24.
//

import Foundation

class BusinessModel {
    var id: Int
    var name: String
    var slug: String
    var header: String
    var logo: String
    var schedule: [ScheduleModel]
    var timezone: String
    var favorite: Bool
    var menusCount: Int
    var availableMenusCount: Int
    var menusSharedCount: Int
    var availableMenusSharedCount: Int
    var distance: Int
    var deliveryZone: Int
    var minimum: Int
    var deliveryPrice: Int
    var deliveryName: String
    var open: Bool
    var today: ScheduleModel
    var zones: [ZoneModel]
    
    init(_ dic: [String: Any]) {
        id = dic["id"] as? Int ?? 0
        name = dic["name"] as? String ?? ""
        slug = dic["slug"] as? String ?? ""
        header = dic["header"] as? String ?? ""
        logo = dic["logo"] as? String ?? ""
        schedule = (dic["schedule"] as? [[String: Any]] ?? []).compactMap { ScheduleModel($0) }
        timezone = dic["timezone"] as? String ?? ""
        favorite = dic["favorite"] as? Bool ?? false
        menusCount = dic["menus_count"] as? Int ?? 0
        availableMenusCount = dic["available_menus_count"] as? Int ?? 0
        menusSharedCount = dic["menus_shared_count"] as? Int ?? 0
        availableMenusSharedCount = dic["available_menus_shared_count"] as? Int ?? 0
        distance = dic["distance"] as? Int ?? 0
        deliveryZone = dic["delivery_zone"] as? Int ?? 0
        minimum = dic["minimum"] as? Int ?? 0
        deliveryPrice = dic["delivery_price"] as? Int ?? 0
        deliveryName = dic["delivery_name"] as? String ?? ""
        open = dic["open"] as? Bool ?? false
        today = ScheduleModel(dic["today"] as? [String: Any] ?? [:])
        zones = (dic["zones"] as? [[String: Any]] ?? []).compactMap { ZoneModel($0) }
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
