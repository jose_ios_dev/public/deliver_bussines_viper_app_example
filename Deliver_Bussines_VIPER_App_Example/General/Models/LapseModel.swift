//
//  LapseModel.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class LapseModel {
    var open: TimeModel
    var close: TimeModel
    
    init(_ dic: [String: Any]) {
        open = TimeModel(dic["open"] as? [String: Any] ?? [:])
        close = TimeModel(dic["close"] as? [String: Any] ?? [:])
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
