//
//  BusinessCataloguePresenter.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation
import UIKit

class BusinessCataloguePresenter: PresenterProtocol {
    weak var view: ViewProtocol?
    
    var wireframe: WireFrameProtocol?
    
    var interactor: InteractorProtocol?
    
    func observerError(_ error: String) {
        (self.view as? DeliveryViewController)?.observerError(NSLocalizedString(error, comment: error))
    }
    
    func showMessage(_ message: String, actions: [UIAlertAction], completion: (() -> Void)?) {
        (self.view as? DeliveryViewController)?.showMessage(message, actions: actions, completion: completion)
    }
    
    func getBusinesses(controller: DeliveryViewController?) {
        (self.interactor as? BusinessCatalogueInteractor)?.getBusinesses(controller: controller)
    }
    
    func setBusinesses(businesses:[BusinessModel]?) {
        (self.view as? BusinessCatalogueViewController)?.setBusinesses(businesses: businesses)
    }
    
    func goToBusinessDetails(navigationController: UINavigationController?, business:BusinessModel?) {
        (self.wireframe as? BusinessCatalogueWireframe)?.goToBusinessDetails(navigationController: navigationController, business: business)
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
