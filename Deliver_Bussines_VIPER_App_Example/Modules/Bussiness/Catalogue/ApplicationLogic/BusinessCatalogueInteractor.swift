//
//  BusinessCatalogueInteractor.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class BusinessCatalogueInteractor: InteractorProtocol {
    weak var presenter: PresenterProtocol?
    
    func observerError(_ error: String) {
        (self.presenter as? BusinessCataloguePresenter)?.observerError(NSLocalizedString(error, comment: error))
    }
    
    func getBusinesses(controller: DeliveryViewController?) {
        DeliverDataStore(controller).getBussines(correctAnswer: self.correctBusinessesAnswer(_:), errorAnswer: self.observerError(_:))
    }
    
    func correctBusinessesAnswer(_ dic:[String:Any]) {
        
        let results = dic["result"] as? [[String:Any]] ?? [[:]]
        var businesses:[BusinessModel] = results.compactMap { BusinessModel($0) }
        (self.presenter as? BusinessCataloguePresenter)?.setBusinesses(businesses: businesses)
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
