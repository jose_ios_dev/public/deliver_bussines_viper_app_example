//
//  BusinessCatalogueWireframe.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation
import UIKit

class BusinessCatalogueWireframe: WireFrameProtocol {
    weak var presenter: PresenterProtocol?
    
    func pushView(navigationController: UINavigationController, view: UIViewController) {
        navigationController.pushViewController(view, animated: true)
    }
    
    func presentView(sourceController:UIViewController,destinationController:UIViewController, modalPresentationStyle:UIModalPresentationStyle, animated:Bool) {
        let navigator = UINavigationController(rootViewController: destinationController)
        navigator.modalPresentationStyle = modalPresentationStyle
        destinationController.modalPresentationStyle = modalPresentationStyle
        sourceController.present(navigator, animated: animated)
    }
    
    init(){}
    
    init(navigationController: UINavigationController){
        let presenter = BusinessCataloguePresenter()
        let interactor = BusinessCatalogueInteractor()
        let view = BusinessCatalogueViewController(nibName: "BusinessCatalogueViewController", bundle: Bundle.main)
        let wireframe = BusinessCatalogueWireframe()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        
        wireframe.presenter = presenter
        interactor.presenter = presenter
        view.presenter = presenter
        
        let navigation = UINavigationController(rootViewController: view)
        if let scene = UIApplication.shared.connectedScenes.first{
            guard let windowScene = (scene as? UIWindowScene) else { return }
            debugPrint("<<<<<< windowScene: \(windowScene)")
            let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
            
            window.windowScene = windowScene
            window.rootViewController = navigation
            window.makeKeyAndVisible()
            view.appDelegate.window = window
        }
        
        
        
    }
    
    func goToBusinessDetails(navigationController:UINavigationController?, business:BusinessModel?) {
        _ = BusinessDetailsWireframe(navigationController: navigationController, business: business)
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
