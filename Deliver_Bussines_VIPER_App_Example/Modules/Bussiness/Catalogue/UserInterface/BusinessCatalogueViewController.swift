//
//  BusinessCatalogueViewController.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import UIKit

class BusinessCatalogueViewController: DeliveryViewController {
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var businesses:[BusinessModel?] = [] {
        didSet{
            self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initController()
    }

    private func initController(){
        self.setControllerText()
        self.initTableView()
    }
    
    private func setControllerText(){
        self.lbltitle.text = "CatalogueModuleTitle".localizable()
        self.navigationItem.title = "CatalogueModuleTitle".localizable()
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }

}

extension BusinessCatalogueViewController:UITableViewDelegate,UITableViewDataSource{
    
    func setBusinesses(businesses:[BusinessModel]?) {
        self.businesses = businesses ?? []
        self.removeLoader()
    }
    
    func getData() {
        (self.presenter as? BusinessCataloguePresenter)?.getBusinesses(controller: self)
    }
    
    func initTableView(){
        self.registerTableViewCells()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.backgroundColor = .clear
        self.getData()
    }
    
    func registerTableViewCells() {
        self.tableView.register(UINib(nibName: "BusinessCatalogueTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "BusinessCatalogueTableViewCell")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.businesses.count
        default:
            return self.businesses.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case 0:
            let cell:BusinessCatalogueTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "BusinessCatalogueTableViewCell", for: indexPath) as! BusinessCatalogueTableViewCell
            cell.fillCell(business: self.businesses[indexPath.row], indexPath: indexPath, presenter: self.presenter)
            return cell
        default:
            let cell:BusinessCatalogueTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "BusinessCatalogueTableViewCell", for: indexPath) as! BusinessCatalogueTableViewCell
            cell.fillCell(business: self.businesses[indexPath.row], indexPath: indexPath, presenter: self.presenter)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        (self.presenter as? BusinessCataloguePresenter)?.goToBusinessDetails(navigationController: self.navigationController, business: self.businesses[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 80
        default:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
        }
    }
    
}
