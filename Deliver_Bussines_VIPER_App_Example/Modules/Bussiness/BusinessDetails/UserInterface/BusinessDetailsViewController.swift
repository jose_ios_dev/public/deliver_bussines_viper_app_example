//
//  BusinessDetailsViewController.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import UIKit

class BusinessDetailsViewController: DeliveryViewController {
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblIsOpen: UILabel!
    @IBOutlet weak var imgIsFavorite: UIImageView!
    @IBOutlet weak var lblTimeZone: UILabel!
    @IBOutlet weak var lblMenusCount: UILabel!
    @IBOutlet weak var lblAvailableMenus: UILabel!
    @IBOutlet weak var lblMenusSharedCount: UILabel!
    @IBOutlet weak var lblDeliveryZone: UILabel!
    @IBOutlet weak var lblDeliberyName: UILabel!
    var business:BusinessModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initController()
    }
    
    private func initController() {
        (self.presenter as? BusinessDetailsPresenter)?.getBusiness()
        self.setControllerText()
    }
    
    private func setControllerText(){
        self.navigationItem.title = "DetailsModuleTitle".localizable()
    }
    
    func setBusiness(business:BusinessModel?) {
        self.business = business
        self.imgHeader.imageFromUrl(urlString: business?.header ?? "", force: false, placeholder: nil)
        self.imgHeader.alpha = 0.4
        self.imgLogo.imageFromUrl(urlString: business?.logo ?? "", force: false, placeholder: UIImage(systemName: "photo.circle"))
        self.lblName.text = business?.name ?? ""
        self.lblDistance.text = String(format: "distance".localizable(), String(business?.distance ?? 0))
        self.lblIsOpen.text = (business?.open ?? false) ? "open".localizable() : "closed".localizable()
        self.imgIsFavorite.image = (business?.favorite ?? false) ? UIImage(systemName: "star.fill") : UIImage(systemName: "star")
        self.lblTimeZone.text = String(format: "timezonedetail".localizable(), business?.timezone ?? "")
        self.lblMenusCount.text = String(format: "MenuCount".localizable(), String(business?.menusCount ?? 0))
        self.lblAvailableMenus.text = String(format: "AvailableMenus".localizable(), String(business?.availableMenusCount ?? 0))
        self.lblMenusSharedCount.text = String(format: "sharedMenus".localizable(), String(business?.menusSharedCount ?? 0))
        self.lblDeliveryZone.text = String(format: "deliveryZone".localizable(), String(business?.deliveryZone ?? 0))
        self.lblDeliberyName.text = String(format: "deliveryName".localizable(), business?.deliveryName ?? "")
        
    }


    deinit {
        debugPrint("<<<\(self)>>>")
    }

}
