//
//  BusinessDetailsPresenter.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation
import UIKit

class BusinessDetailsPresenter: PresenterProtocol {
    weak var view: ViewProtocol?
    
    var wireframe: WireFrameProtocol?
    
    var interactor: InteractorProtocol?
    
    func observerError(_ error: String) {
        (self.view as? DeliveryViewController)?.observerError(NSLocalizedString(error, comment: error))
    }
    
    func showMessage(_ message: String, actions: [UIAlertAction], completion: (() -> Void)?) {
        (self.view as? DeliveryViewController)?.showMessage(message, actions: actions, completion: completion)
    }
    
    func getBusiness() {
        (self.interactor as? BusinessDetailsInteractor)?.getBusiness()
    }
    
    func setBusiness(business:BusinessModel?) {
        (self.view as? BusinessDetailsViewController)?.setBusiness(business: business)
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
