//
//  BusinessDetailsInteractor.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import Foundation

class BusinessDetailsInteractor: InteractorProtocol {
    weak var presenter: PresenterProtocol?
    var business:BusinessModel?
    
    func observerError(_ error: String) {
        (self.presenter as? BusinessDetailsPresenter)?.observerError(NSLocalizedString(error, comment: error))
    }
    
    func getBusiness() {
        (self.presenter as? BusinessDetailsPresenter)?.setBusiness(business: self.business)
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }
}
