//
//  ViewController.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 17/03/24.
//

import UIKit

class ViewController: DeliveryViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.goToBusinessList()
    }
    
    func goToBusinessList() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            _ = BusinessCatalogueWireframe(navigationController: self.navigationController ?? UINavigationController())
        })
    }
    
    deinit {
        debugPrint("<<<\(self)>>>")
    }

}

