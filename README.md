# Deliver_Bussines_VIPER_App_Example

Deliver_Bussines_VIPER_App_Example is a simple application developed in UIKit and Swift that demonstrates the use of the VIPER design pattern.

## Features

- Displays a list of businesses with their logo, name, and some data.
- Allows viewing some details of each business.

## Compatibility

Deliver_Bussines_VIPER_App_Example is compatible with devices running iOS 15 and later.

## VIPER Design Pattern

Deliver_Bussines_VIPER_App_Example follows the VIPER design pattern to separate the concerns of the application into distinct layers:

- **View:** Presents the user interface and handles user interactions.
- **Interactor:** Contains the business logic and interacts with the data layer.
- **Presenter:** Formats the data to be displayed in the view and handles user inputs.
- **Entity:** Represents the data objects used by the application.
- **Router:** Manages navigation between modules.

## Video Tutorials

- [Understanding VIPER Design Pattern](https://youtu.be/RWuyIUokhx0)
- [Deliver_Bussines_VIPER_App_Example Demo](https://youtu.be/qc76Zq6Isqg)

## Usage Instructions

1. Clone or download the Deliver_Bussines_VIPER_App_Example repository.
2. Open the Deliver_Bussines_VIPER_App_Example.xcworkspace file in Xcode.
3. Run the application on the iOS simulator or a physical device.
4. The application will display a list of businesses.
5. Tap on any business to view its details.
6. Explore the functionality of Deliver_Bussines_VIPER_App_Example and learn about the VIPER design pattern in iOS!

## License

Deliver_Bussines_VIPER_App_Example is released under the [MIT License](LICENSE).

## Authors

- [José Antonio Caballero Martínez](https://gitlab.com/JoseAntonioCaballero)
