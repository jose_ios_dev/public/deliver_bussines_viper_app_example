//
//  BusinessCatalogueTableViewCell.swift
//  Deliver_Bussines_VIPER_App_Example
//
//  Created by José Caballero on 18/03/24.
//

import UIKit

class BusinessCatalogueTableViewCell: UITableViewCell {
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistanceKM: UILabel!
    @IBOutlet weak var lblIsOpen: UILabel!
    @IBOutlet weak var imgIsFavorite: UIImageView!
    weak var presenter:PresenterProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillCell(business:BusinessModel?, indexPath:IndexPath, presenter:PresenterProtocol?) {
        self.presenter = presenter
        self.tag = indexPath.row
        
        self.imgHeader.imageFromUrl(urlString: business?.header ?? "", force: false, placeholder: nil)
        self.imgHeader.alpha = 0.3
        self.imgLogo.imageFromUrl(urlString: business?.logo ?? "", force: false, placeholder: UIImage(systemName: "photo.circle"))
        self.lblName.text = business?.name ?? ""
        self.lblDistanceKM.text = String(format: "distance".localizable(), String(business?.distance ?? 0))
        self.lblIsOpen.text = (business?.open ?? false) ? "open".localizable() : "closed".localizable()
        self.imgIsFavorite.image = (business?.favorite ?? false) ? UIImage(systemName: "star.fill") : UIImage(systemName: "star")
        
    }
    
}
